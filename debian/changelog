palettable (3.3.3-1) unstable; urgency=medium

  * Prepare upload to unstable.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Wed, 14 Jun 2023 20:56:31 -0300

palettable (3.3.3-1~exp1) experimental; urgency=medium

  * New upstream version.
  * d/control: Build with pybuild's pyproject plugin. Add
    pybuild-plugin-pyproject as Build Depends.
  * d/control: Run wrap-and-sort. Also in d/tests/control file.
  * d/control: Update Homepage url to be according to the pyproject.toml file.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Mon, 05 Jun 2023 07:21:36 -0300

palettable (3.3.1-1~exp1) experimental; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 3.3.0-3.

  [ Emmanuel Arias ]
  * New upstream version.
  * d/control: Update copyright year for debian/* files.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Sat, 15 Apr 2023 17:24:34 -0300

palettable (3.3.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Refer to common license file for CC0-1.0.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [Emmanuel Arias]
  * d/control: Bump Standards-Version to 4.6.2 (from 4.5.0; no further changes
    needed).
  * d/copyright: Update copyright year for debian/* files.
    - Use expat license name instead of MIT.
  * d/watch: Bump version to 4.
  * d/rules: Check for DEB_BUILD_OPTIONS in override_dh_auto_test.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Thu, 29 Dec 2022 01:34:33 -0300

palettable (3.3.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andrius Merkys ]
  * Adding 'Rules-Requires-Root: no'.

 -- Andrius Merkys <merkys@debian.org>  Thu, 22 Oct 2020 00:16:28 -0400

palettable (3.3.0-1) unstable; urgency=medium

  * Initial release (Closes: #962340)

 -- Emmanuel Arias <eamanu@yaerobi.com>  Mon, 08 Jun 2020 12:53:36 -0300
